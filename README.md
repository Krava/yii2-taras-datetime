Test DateTimeWidget 
==========================
Yii2 extension for [Bootstrap 3 Datepicker](https://eonasdan.github.io/bootstrap-datetimepicker/) 

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist tarasvendor/yii2-taras-datetime:"dev-master"
```

or add

```
"tarasvendor/yii2-taras-datetime":"dev-master"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```
<?= DateTimeWidget::widget(['model' => $model, 'attribute' => 'created_at']); ?>
```

