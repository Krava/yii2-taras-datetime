<?php

namespace taras\datetimr;

use taras\datetimr\assets\DateTimeWidgetAsset;
use yii\base\Model;
use yii\helpers\Html;
use Yii;
use yii\web\View;

class DateTimeWidget extends \yii\base\Widget
{
    public $model;
    public $attribute;
    public $value;
    public $options = ['class' => 'form-control datetime',
//        'id' => ''
    ];
    public $optionsWidget = [
        'format' => 'YYYY-MM-DD HH:mm',
        'locale' => 'ru'
    ];

    public function init()
    {
        parent::init();

        $view = $this->getView();
        DateTimeWidgetAsset::register($view);
    }

    public function run()
    {
        $idName = Html::getInputId($this->model, $this->attribute);

        $html = "<div class='form-group'>
                    <div class='input-group date' id='{$idName}'>
                    " . $this->renderWidget() . "
                        <span class='input-group-addon'>
                            <span class='glyphicon glyphicon-calendar'></span>
                        </span>
                    </div>
                </div>";

$js = <<<JS
        $(document).ready(function() {
              $('#{$idName}').datetimepicker({
                  'allowInputToggle': true,
                 'useCurrent': false,
                'format' : '{$this->optionsWidget['format']}',
                'locale': '{$this->optionsWidget['locale']}'
              });
            })
JS;

        Yii::$app->view->registerJs($js, View::POS_END);
        return $html;
    }

    /**
     * @return string
     */
    protected function renderWidget()
    {
        $contents = [];

        // get formatted date value
        if ($this->hasModel()) {
            $value = Html::getAttributeValue($this->model, $this->attribute);
        } else {
            $value = $this->value;
        }

        $options = $this->options;
        $options['value'] = $value;
        // render a text input
        if ($this->hasModel()) {
            $contents[] = Html::activeTextInput($this->model, $this->attribute, $options);
        } else {
            $contents[] = Html::textInput($this->name, $value, $options);
        }
        return implode("\n", $contents);
    }

    /**
     * @return bool whether this widget is associated with a data model.
     */
    protected function hasModel()
    {
        return $this->model instanceof Model && $this->attribute !== null;
    }
}
