<?php

namespace taras\datetimr\assets;

use yii\web\AssetBundle;

class DateTimeWidgetAsset extends AssetBundle
{
    public $basePath = __DIR__.'/';
    public $sourcePath = __DIR__.'/';
    public $js = [
        'moment-with-locales.js',
        'bootstrap-datetimepicker.js',
    ];
    public $css = [
        'bootstrap-datetimepicker.css',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset'
    ];
}
